from modules.interface_module import Module
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram.error import (TelegramError, Unauthorized, BadRequest,
                            TimedOut, ChatMigrated, NetworkError)
import logging


class ErrorModule(Module):

    def __init__(self):
        super().__init__()

    def init_module(self, telegram_bot):
        super().init_module(telegram_bot)
        logging.info("Init {} ...".format(self.__class__.__name__))
        # add handler which keeps track of unknown commands
        unknown_cmds_handler = MessageHandler(Filters.command, self.unknown_commands)
        telegram_bot.dispatcher.add_handler(unknown_cmds_handler)
        # add error handler
        telegram_bot.dispatcher.add_error_handler(self.handle_error)

    def unknown_commands(self, update, context):
        logging.info("Unknown cmd by User: {} with ID: {} Message: {}".format(
                     update.message.from_user.first_name,
                     update.message.from_user.id,
                     update.message.text))

    def handle_error(self, update, context):
        # https://github.com/python-telegram-bot/python-telegram-bot/wiki/Exception-Handling
        try:
            raise context.error
        except Unauthorized:
            # remove update.message.chat_id from conversation list
            logging.error("Unauthorized: {}".format(context.error))
        except BadRequest:
            # handle malformed requests - read more below!
            logging.error("BadRequest: {}".format(context.error))
        except TimedOut:
            # handle slow connection problems
            logging.error("TimedOut: {}".format(context.error))
        except NetworkError:
            # handle other connection problems
            logging.error("NetworkError: {}".format(context.error))
        except ChatMigrated as e:
            # the chat_id of a group has changed, use e.new_chat_id instead
            logging.error("ChatMigrated: {}".format(context.error))
        except TelegramError:
            # handle all other telegram related errors
            logging.error("TelegramError: {}".format(context.error))


