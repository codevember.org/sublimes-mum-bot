from modules.interface_module import Module
from telegram.ext import CommandHandler
from telegram import ParseMode
import subprocess
import os
import logging


class GitChangelogModule(Module):

    def __init__(self, path_to_git=None):
        super().__init__()
        self._last_n_commits = 10
        self._init_path_to_git(path_to_git)

    def _init_path_to_git(self, path_to_git):
        if path_to_git:
            self._path_to_git = path_to_git
        else:
            path_to_project_dir_git = os.path.join(os.path.dirname(os.path.dirname(__file__)), ".git")
            if os.path.exists(path_to_project_dir_git):
                self._path_to_git = path_to_project_dir_git
            else:
                raise Exception("No .git repository found!")

    def init_module(self, telegram_bot):
        super().init_module(telegram_bot)
        logging.info("Init {} ...".format(self.__class__.__name__))
        changelog_handler = CommandHandler('changelog', self.send_changelog)
        telegram_bot.dispatcher.add_handler(changelog_handler)

    def get_help_description(self):
        return '/changelog - Shows the changelog of the last 10 commits'

    def get_git_changelog(self):
        cmd = ['git', 'log', '--pretty=format:"%h %ar: %s"', '-n {}'.format(self._last_n_commits)]
        cmd_output = subprocess.run(cmd, cwd=self._path_to_git, capture_output=True)
        return cmd_output.stdout.decode('utf-8').replace('"', '')

    def get_changelog(self):
        git_changelog = self.get_git_changelog()
        git_changelogs = git_changelog.split("\n")
        changes = ""
        for commit in git_changelogs:
            commit_id = commit[:7]
            commit_msg = commit[8:].split(":")
            changes += "*[{}]* ({}) \n - {} \n".format(commit_id, commit_msg[0], commit_msg[1][1:])
        return changes

    def send_changelog(self, update, context):
        logging.info("Send Changelog called ...")

        if not self.is_user_allowed_to_chat(update.message.chat_id):
            return

        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=self.get_changelog(),
            disable_notification=True,
            parse_mode=ParseMode.MARKDOWN)
