from modules.interface_module import Module
from telegram.ext import MessageHandler
from telegram.ext import Filters
import logging


class GifModule(Module):

    def __init__(self):
        super().__init__()

    def init_module(self, telegram_bot):
        super().init_module(telegram_bot)
        logging.info("Init {} ...".format(self.__class__.__name__))
        gif_handler = MessageHandler(Filters.document.gif, self.detect_gif)
        telegram_bot.dispatcher.add_handler(gif_handler)

    def detect_gif(self, update, context):
        # track statistic of group
        if self.is_equal_group_chat_id(update.message.chat_id):
            self.statistic.add_one_gif_count()


