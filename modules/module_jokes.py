from modules.interface_module import Module
from telegram.ext import CommandHandler
import random
import importlib
from resources import jokes
import logging


class JokesModule(Module):

    def __init__(self):
        super().__init__()
        self.jokes = jokes.jokes_list

    def init_module(self, telegram_bot):
        super().init_module(telegram_bot)
        logging.info("Init {} ...".format(self.__class__.__name__))
        joke_handler = CommandHandler('joke', self.send_joke)
        telegram_bot.dispatcher.add_handler(joke_handler)

    def check_daily(self, context):
        importlib.reload(jokes)
        self.jokes = None
        self.jokes = jokes.jokes_list

    def get_help_description(self):
        return '/joke - Unleash the fun'

    def send_joke(self, update, context):
        logging.info("Send Joke called ...")

        # if not given chat id return
        if not self.is_user_allowed_to_chat(update.message.chat_id):
            return

        # track statistic of group
        if self.is_equal_group_chat_id(update.message.chat_id):
            self.statistic.add_one_joke_count()

        # return random joke from list
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=random.choice(self.jokes),
            disable_notification=True)

