from modules.interface_module import Module
from trello.trello import Trello
from telegram.ext import MessageHandler
from telegram.ext import Filters
import importlib
from resources import reactions
from datetime import datetime
import re
import json
import logging


class TextModule(Module):

    def __init__(self):
        super().__init__()
        self.trello = Trello()
        self.reactions_dict = reactions.reactions_dict

    def init_module(self, telegram_bot):
        super().init_module(telegram_bot)
        logging.info("Init {} ...".format(self.__class__.__name__))
        text_handler = MessageHandler(Filters.text, self.filter_text)
        telegram_bot.dispatcher.add_handler(text_handler)

    def check_daily(self, context):
        importlib.reload(reactions)
        self.reactions_dict = None
        self.reactions_dict = reactions.reactions_dict

    def send_reaction_message(self, context, update):
        replay_msg_list = self.get_replay_msgs_list(update.message.text)

        if len(replay_msg_list) == 0:
            return

        # track reactions
        if self.is_equal_group_chat_id(update.message.chat_id):
            self.statistic.add_one_reactions_count()

        # replay reactions to message
        for replay_msg in replay_msg_list:
            if replay_msg.lower().endswith(".jpg"):
                context.bot.send_photo(chat_id=update.message.chat_id,
                                       photo=replay_msg,
                                       disable_notification=True)
            elif replay_msg.lower().endswith(".gif"):
                context.bot.send_animation(chat_id=update.message.chat_id,
                                           animation=replay_msg,
                                           disable_notification=True)
            else:
                context.bot.send_message(chat_id=update.message.chat_id,
                                         text=replay_msg,
                                         disable_notification=True)

    def get_replay_msgs_list(self, msg_content):
        replay_msg_list = []
        msg_content = str(msg_content).lower()
        for reaction, msg_replay in self.reactions_dict.items():
            if reaction.startswith("#"):
                regex_reaction = r'\B{}\b'.format(reaction)
            else:
                regex_reaction = r'\b{}\b'.format(reaction)
            if re.search(regex_reaction, msg_content):
                replay_msg_list.append(msg_replay)
        return replay_msg_list

    @staticmethod
    def contains_trello_hashtag(msg):
        regex_trello = '\B\#trello\\b'
        return re.search(regex_trello, msg) is not None

    @staticmethod
    def get_urls_from_msg(msg):
        regex_url = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+" \
                    r"|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
        urls = re.findall(regex_url, msg)
        return [url[0] for url in urls]

    def add_urls_to_card(self, urls, response):
        response_content_json = json.loads(response.content.decode('utf-8'))
        card_id = response_content_json.get('id')
        for url in urls:
            response_att = self.trello.add_url_attachment_to_card(card_id, url)
            if response_att.status_code == 200:
                logging.info("... successfully added url attachment!")
            else:
                logging.error("... something went wrong adding attachment return_code = {}"
                              .format(response_att.status_code))

    def create_trello_card_direct_msg(self, msg, desc):
        urls = self.get_urls_from_msg(msg.text)
        if len(urls) == 0:
            return
        date_str = datetime.now().strftime("%Y-%m-%d")
        response = self.trello.create_card(date_str, desc)
        if response.status_code == 200:
            logging.info("... successfully created Trello Card!")
            self.add_urls_to_card(urls, response)
        else:
            logging.error("... something went wrong return_code = {}".format(response.status_code))

    def create_trello_card_reply_msg(self, msg, desc):
        if not msg.reply_to_message:
            return
        self.create_trello_card_direct_msg(msg.reply_to_message, desc)

    def create_trello_card(self, msg, desc):
        self.create_trello_card_direct_msg(msg, desc)
        self.create_trello_card_reply_msg(msg, desc)

    def save_on_trello(self, msg):
        if self.contains_trello_hashtag(msg.text):
            self.create_trello_card(msg, "")

    def filter_text(self, update, context):

        # prevent some telegram errors ?!
        if update is None:
            logging.error("Telegram Update is None")
            return

        if update.message is None:
            logging.error("Telegram Update.Message is None")
            return

        # check chat id
        if not self.is_user_allowed_to_chat(update.message.chat_id):
            return

        # track statistic of group and check if it's a marked with #trello
        if self.is_equal_group_chat_id(update.message.chat_id):
            self.statistic.add_one_msg_count()
            self.save_on_trello(update.message)

        # check message content
        self.send_reaction_message(context, update)
