import abc


class Module(object):

    def __init__(self):
        self._group_chat_id = None
        self._admin_chat_id = None
        self._callback_func_check_user_list = None
        self._statistic = None

    @abc.abstractmethod
    def init_module(self, telegram_bot):
        self._statistic = telegram_bot.statistic

    def check_daily(self, context):
        pass

    def get_help_description(self):
        return ""

    @property
    def statistic(self):
        return self._statistic

    def set_chat_ids(self, group_chat_id, admin_chat_id, callback_func_check_user_list):
        self._group_chat_id = group_chat_id
        self._admin_chat_id = admin_chat_id
        self._callback_func_check_user_list = callback_func_check_user_list

    def is_equal_group_chat_id(self, chat_id):
        return self._group_chat_id == chat_id

    def is_equal_admin_chat_id(self, chat_id):
        return self._admin_chat_id == chat_id

    def is_equal_group_or_admin_chat_id(self, chat_id):
        return (self._group_chat_id == chat_id) or \
               (self._admin_chat_id == chat_id)

    def is_user_allowed_to_chat(self, chat_id):
        return (self._group_chat_id == chat_id) or \
               (self._admin_chat_id == chat_id) or \
               (self._callback_func_check_user_list(chat_id))
