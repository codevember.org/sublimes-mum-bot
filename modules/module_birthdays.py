from modules.interface_module import Module
from handler.birthday_handler import BirthdayHandler
from telegram import (InlineKeyboardMarkup, InlineKeyboardButton)
from telegram import ParseMode
from telegram.ext import CallbackQueryHandler
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from core.common import get_current_datetime_of_timezone
from enum import Enum
import logging


class BirthdaysState(Enum):
    BIRTHDAYS_LIST  = 1


class BirthdaysModule(Module):

    def __init__(self):
        super().__init__()
        # set up inline keyboard
        self.reply_markup = InlineKeyboardMarkup(
            [[InlineKeyboardButton("Prev", callback_data='b_prev'),
              InlineKeyboardButton("Next", callback_data='b_next')],
             [InlineKeyboardButton("Save", callback_data='b_save'),
              InlineKeyboardButton("Quit", callback_data='b_quit')]])

        self.birthday_handler = None
        self.database_users = None

    def init_module(self, telegram_bot):
        super().init_module(telegram_bot)
        logging.info("Init {} ...".format(self.__class__.__name__))
        # init birthdays handler
        conversation_handler = ConversationHandler(
            entry_points=[CommandHandler('birthdays', self.birthdays, pass_args=True)],
            states={BirthdaysState.BIRTHDAYS_LIST: [CallbackQueryHandler(self.update)]},
            fallbacks=[CommandHandler('birthdays', self.birthdays, pass_args=True)])
        telegram_bot.dispatcher.add_handler(conversation_handler)
        self.birthday_handler = BirthdayHandler(telegram_bot.data)
        self.database_users = telegram_bot.database_users

    def check_daily(self, context):
        self.happy_birthday(context)

    def get_help_description(self):
        help_desc = '/birthdays - Show Birthday list \n'
        help_desc += '/birthdays <name> - Show birthday results found via user input'
        return help_desc

    def birthdays(self, update, context):
        logging.info("Birthdays: birthdays() by User: {}".format(update.message.from_user.first_name))
        # check chat id
        if not self.is_user_allowed_to_chat(update.message.chat_id):
            return None

        if len(context.args) > 0:
            # handle user input /birthdays <name>
            self.handle_user_input(update, context)
            return ConversationHandler.END
        else:
            # no args, show birthdays list
            self.show_birthdays_list(update, context)
            return BirthdaysState.BIRTHDAYS_LIST

    def send_inform_msg(self, context):
        # users tpl list with (id, name)
        users = self.database_users.get_all_user_data()
        if len(users) == 0:
            return None

        text = ""
        for user_tpl in users:
            text += "[{}](tg://user?id={}) ".format(user_tpl[1], user_tpl[0])

        context.bot.send_message(chat_id=context.job.context, text=text, parse_mode=ParseMode.MARKDOWN)

    def happy_birthday(self, context):
        # get birthday person
        today = get_current_datetime_of_timezone()
        birthday_persons = self.birthday_handler.get_birthday_persons(today)
        logging.info("HappyBirthday daily: {}".format(today))

        # nobody has birthday
        if len(birthday_persons) == 0:
            return None

        # concat birthday text
        image_caption = ""
        for person in birthday_persons:
            image_caption += "Happy {}th birthday {} 🎂 \n".format(person.age, person.name)

        # check if new images are set and check if url is valid
        self.birthday_handler.update_birthday_images()
        # send text and photo to chat
        image_url = self.birthday_handler.get_random_birthday_image_url()
        if image_url is None:
            context.bot.send_message(chat_id=context.job.context, text=image_caption, parse_mode=ParseMode.MARKDOWN)
        else:
            context.bot.send_photo(chat_id=context.job.context, photo=image_url, caption=image_caption)

        # inform users on notification list
        self.send_inform_msg(context)
        # update list to new member if someone had birthday
        self.birthday_handler.update_current_index()

        return ConversationHandler.END

    def update(self, update, context):
        # get resources from query
        data = update.callback_query.data
        if data == 'b_prev':
            self.get_prev(update, context)
        elif data == 'b_next':
            self.get_next(update, context)
        elif data == 'b_save':
            self.save(update, context)
        elif data == 'b_quit':
            self.quit(update, context)
        else:
            pass

    def quit(self, update, context):
        query = update.callback_query
        context.bot.delete_message(
            chat_id=query.message.chat_id,
            message_id=query.message.message_id)
        context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def get_prev(self, update, context):
        query = update.callback_query
        person = self.birthday_handler.get_prev_birthday_person()
        text = self.parse_next_birthday_text(person)
        context.bot.edit_message_text(
            text=text,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=self.reply_markup)
        context.bot.answer_callback_query(update.callback_query.id)
        return BirthdaysState.BIRTHDAYS_LIST

    def get_next(self, update, context):
        query = update.callback_query
        person = self.birthday_handler.get_next_birthday_person()
        text = self.parse_next_birthday_text(person)
        context.bot.edit_message_text(
            text=text,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=self.reply_markup)
        context.bot.answer_callback_query(update.callback_query.id)
        return BirthdaysState.BIRTHDAYS_LIST

    def save(self, update, context):
        query = update.callback_query
        context.bot.edit_message_text(
            text=query.message.text,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id)
        context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def parse_next_birthday_text(self, person):
        year = self.birthday_handler.get_year(get_current_datetime_of_timezone(), person.birthday_datetime)
        # return markdown string
        return "*Next Birthday Party*\n" \
               "Name: {0}\n" \
               "Current Age: {1}\n" \
               "Birthday: {2}\n\n" \
               "*{0}* will be {3} years old on {4}".format(person.name, person.age, person.birthday_str, person.age+1,
                                                           person.birthday_str[:-5] + "-" + str(year))

    def handle_user_input(self, update, context):
        logging.info("Birthdays: handle_user_input() args={}".format(context.args))
        # filter just for first keyword
        person_list = self.birthday_handler.get_birthday_by_name(name=context.args[0])

        # nobody was found
        if len(person_list) == 0:
            text = "No results with *{}* have been found.".format(context.args[0])
        else:
            spacer = ""
            if len(person_list) > 1:
                spacer = "\n\n"

            text = ""
            for person in person_list:
                year = self.birthday_handler.get_year(get_current_datetime_of_timezone(), person.birthday_datetime)
                text += "*{0}*\n" \
                        "Current Age: {1}\n" \
                        "Birthday: {2}\n\n" \
                        "*{0}* will be {3} years old on {4}{5}".format(person.name, person.age, person.birthday_str,
                                                                       person.age + 1,
                                                                       person.birthday_str[:-5] + "-" + str(year),
                                                                       spacer)
        context.bot.send_message(
            text=text,
            chat_id=update.message.chat_id,
            disable_notification=True,
            parse_mode=ParseMode.MARKDOWN)

    def show_birthdays_list(self, update, context):
        self.birthday_handler.reset_current_index()
        person = self.birthday_handler.get_current_birthday_person()
        text = self.parse_next_birthday_text(person)
        update.message.reply_text(text=text, reply_markup=self.reply_markup, parse_mode=ParseMode.MARKDOWN)
