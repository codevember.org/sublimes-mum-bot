from core.common import get_current_datetime_of_timezone
import datetime


class Person(object):

    def __init__(self, name: str, birthday_str: str):
        self._name = name
        self._birthday_str = birthday_str
        self._birthday_datetime = datetime.datetime.strptime(birthday_str, '%d-%m-%Y')
        self._age = self.compute_current_age(self._birthday_datetime)

    @property
    def name(self) -> str:
        return self._name

    @property
    def birthday_str(self) -> str:
        return self._birthday_str

    @property
    def birthday_datetime(self) -> datetime.datetime:
        return self._birthday_datetime

    @property
    def birthday_year(self) -> int:
        return self._birthday_datetime.year

    @property
    def birthday_month(self) -> int:
        return self._birthday_datetime.month

    @property
    def birthday_day(self) -> int:
        return self._birthday_datetime.day

    def update_age(self, year: int) -> None:
        self._age = (year - self._birthday_datetime.year)

    @property
    def age(self) -> int:
        return self._age

    @age.setter
    def age(self, new_age: int) -> None:
        self._age = new_age

    @staticmethod
    def compute_current_age(datetime_birthday: datetime.datetime) -> int:
        datetime_today = get_current_datetime_of_timezone()
        if datetime_today.month < datetime_birthday.month:
            return (datetime_today.year - datetime_birthday.year) - 1
        elif datetime_today.month == datetime_birthday.month:
            if datetime_today.day < datetime_birthday.day:
                return (datetime_today.year - datetime_birthday.year) - 1
        return datetime_today.year - datetime_birthday.year
