from core.person import Person
from typing import List
import datetime
import logging
import time
import sys


class Group(object):

    def __init__(self, data):
        logging.info("Init {}".format(self.__class__.__name__))
        # list of persons
        self._group = []
        if not self.load_group_from_json(data):
            logging.error("Loading data from .json file FAILED")
            sys.exit(0)

    @property
    def persons(self) -> List[Person]:
        return self._group

    def load_group_from_json(self, json) -> bool:
        try:
            start = time.time()
            logging.info("Loading Persons from json file ...")
            for person in json['persons']:
                # todo check valid input
                self._group.append(Person(person['name'], person['birthday']))
            self.sort_group_after_birthdays()
            logging.info("Finished loading ... [{:.4f}ms]".format(time.time()-start))
            return True
        except Exception as e:
            logging.error("Something went wrong loading the resources from .json: {}".format(e))
            return False

    def sort_group_after_birthdays(self, descending=False) -> None:
        self._group = sorted(self._group, reverse=descending,
                             key=lambda person: datetime.datetime.strptime(person.birthday_str[:-5], '%d-%m'))
