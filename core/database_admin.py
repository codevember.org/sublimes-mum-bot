import sqlite3
import logging


class DatabaseAdmin(object):

    def __init__(self, filepath_db):
        logging.info("Init {}".format(self.__class__.__name__))
        self.filepath_db = filepath_db
        self.create_admin_table()

    def create_admin_table(self):
        conn = sqlite3.connect(self.filepath_db)
        c = conn.cursor()
        c.execute("""CREATE TABLE IF NOT EXISTS admin
            (chat_id INTEGER UNIQUE, user_id INTEGER)""")
        conn.commit()
        conn.close()

    def get_admin_data(self, user_id):
        conn = sqlite3.connect(self.filepath_db)
        c = conn.cursor()
        c.execute("SELECT * FROM admin WHERE user_id=?", (user_id, ))
        rows = c.fetchall()
        conn.close()
        return rows

    def is_admin_in_table(self, user_id):
        conn = sqlite3.connect(self.filepath_db)
        c = conn.cursor()
        for _ in c.execute("SELECT user_id FROM admin WHERE user_id=?", (user_id, )):
            conn.close()
            return True
        conn.close()
        return False

    def insert_admin(self, chat_id, user_id):
        try:
            conn = sqlite3.connect(self.filepath_db)
            c = conn.cursor()
            c.execute("INSERT INTO admin(chat_id, user_id) VALUES(?, ?)", (chat_id, user_id))
            conn.commit()
            conn.close()
            return True
        except Exception as e:
            logging.error("Database insert error: {}".format(e))
            return False
