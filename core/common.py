import requests
import logging
import time
import pytz
import datetime
from tzlocal import get_localzone


def is_valid(url):
    logging.info("Check url: {}".format(url))
    start = time.time()

    try:
        request = requests.get(url)
    except Exception as e:
        logging.error(e)
        return False

    if request.status_code != 200:
        logging.error("... url is invalid and will be skipped! Status code {}".format(request.status_code))
        return False
    logging.info("... success [{:.4f}ms]".format(time.time() - start))
    return True


def get_midnight_time_of_timezone(time_zone='Europe/Berlin'):
    today = datetime.date.today()
    zone = get_localzone().zone
    midnight = datetime.datetime.combine(today, datetime.datetime.min.time())
    pytz_timezone = pytz.timezone(time_zone)
    midnight_europe = pytz_timezone.localize(midnight)
    midnight_europe_as_timezone = midnight_europe.astimezone(pytz.timezone(zone))
    return midnight_europe_as_timezone.time()


def get_current_datetime_of_timezone(time_zone='Europe/Berlin'):
    pytz_timezone = pytz.timezone(time_zone)
    return datetime.datetime.now(pytz_timezone)
