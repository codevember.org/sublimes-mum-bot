import sqlite3
import logging


class DatabaseUser(object):

    def __init__(self, filepath_db):
        logging.info("Init {}".format(self.__class__.__name__))
        self.filepath_db = filepath_db
        self.create_user_table()

    def create_user_table(self):
        conn = sqlite3.connect(self.filepath_db)
        c = conn.cursor()
        c.execute("""CREATE TABLE IF NOT EXISTS user
            (chat_id INTEGER UNIQUE, name TEXT)""")
        conn.commit()
        conn.close()

    def is_user_in_table(self, chat_id):
        conn = sqlite3.connect(self.filepath_db)
        c = conn.cursor()
        for _ in c.execute("SELECT chat_id FROM user WHERE chat_id=?", (chat_id, )):
            conn.close()
            return True
        conn.close()
        return False

    def insert_user(self, tpl):
        try:
            conn = sqlite3.connect(self.filepath_db)
            c = conn.cursor()
            c.execute("INSERT INTO user(chat_id, name) VALUES(?, ?)", (tpl[0], tpl[1]))
            conn.commit()
            conn.close()
            return True
        except Exception as e:
            logging.error("Database insert error: {}".format(e))
            return False

    def remove_user(self, tpl):
        try:
            conn = sqlite3.connect(self.filepath_db)
            c = conn.cursor()
            c.execute("DELETE FROM user WHERE chat_id=?", (tpl[0], ))
            conn.commit()
            conn.close()
            return True
        except Exception as e:
            logging.error("Database remove error: {}".format(e))
            return False

    def get_all_user_data(self):
        conn = sqlite3.connect(self.filepath_db)
        c = conn.cursor()
        c.execute("SELECT * FROM user")
        rows = c.fetchall()
        conn.close()
        return rows

