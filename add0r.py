from typing import Dict
import argparse
import json
import sys


def add_user(json_dict: Dict, user: str, birthday: str) -> Dict:
    d = {'name': user, 'birthday': birthday}
    json_dict['persons'].append(d)
    return json_dict


def get_json_content(filename: str) -> Dict:
    with open(filename, 'r') as file:
        json_content = json.load(file)
    return json_content


def save(filename: str, json_content: dict) -> bool:
    try:
        with open(filename, 'w') as file:
            json.dump(json_content, file, indent=2)
    except Exception as e:
        print(e)
        return False
    return True


def main():
    if args.file.endswith('.json'):
        json_content = get_json_content(args.file)
        json_content = add_user(json_content, args.user, args.birthday)
        if save(args.file, json_content):
            return 0
        else:
            return 1
    else:
        raise Exception('Not supported file format. Supported file format is .json')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Helper for adding a person to the birthday list.')
    parser.add_argument('-f', '--file', metavar='file', type=str,
                        help='File .json or .aes to add person', required=True)
    parser.add_argument('-u', '--user', metavar='user', type=str,
                        help='Name of person', required=True)
    parser.add_argument('-b', '--birthday', metavar='birthday', type=str,
                        help='Birthday format DD-MM-YYYY', required=True)
    args = parser.parse_args()
    sys.exit(main())
