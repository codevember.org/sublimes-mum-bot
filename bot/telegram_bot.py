from core.database_admin import DatabaseAdmin
from core.database_user import DatabaseUser
from statistics.statistic import Statistic
from telegram import (InlineKeyboardMarkup, InlineKeyboardButton)
from telegram import ParseMode
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import CallbackQueryHandler
from core.common import get_midnight_time_of_timezone
from core.common import get_current_datetime_of_timezone
import logging
import os
import modules


class TelegramBot(object):

    def __init__(self, token, data):
        # set bot token id and get dispatcher
        self.updater = Updater(token=token, use_context=True, user_sig_handler=self.shutdown_graceful)

        # running jobs
        self.running_jobs_dict = {}

        self.modules = []
        self.yes_no_markup = None
        self.group_chat_id = None
        self.admin_chat_id = None
        self._database_admin = None
        self._database_users = None

        self._data = data
        self._statistic = Statistic()
        self.init_commands()
        self.init_database()
        self.init_modules()

    @property
    def dispatcher(self):
        return self.updater.dispatcher

    @property
    def data(self):
        return self._data

    @property
    def statistic(self):
        return self._statistic

    @property
    def database_users(self):
        return self._database_users

    def init_commands(self):
        dp = self.updater.dispatcher
        # add commands /<function>
        start_handler = CommandHandler('start', self.start, pass_job_queue=True)
        stop_handler = CommandHandler('stop', self.stop)
        help_handler = CommandHandler('help', self.help)
        ping_handler = CommandHandler('ping', self.ping)
        notify_handler = ConversationHandler(
            entry_points=[CommandHandler('notify', self.notify)],
            states={0: [CallbackQueryHandler(self.yes_no_input)]},
            fallbacks=[CommandHandler('notify', self.notify)])

        # yes / no keyboard
        self.yes_no_markup = InlineKeyboardMarkup(
            [[InlineKeyboardButton("Yes", callback_data='yes'),
              InlineKeyboardButton("No", callback_data='no')]])

        report_handler = CommandHandler('report', self.report, pass_args=True)

        # add handler to dispatcher
        dp.add_handler(start_handler)
        dp.add_handler(stop_handler)
        dp.add_handler(help_handler)
        dp.add_handler(ping_handler)
        dp.add_handler(notify_handler)
        dp.add_handler(report_handler)

    def init_database(self):
        # group chat id and admin chat id
        filepath = os.path.dirname(__file__)
        filepath_admin_db = os.path.abspath(os.path.join(filepath, "..", "database", "admin.db"))
        self._database_admin = DatabaseAdmin(filepath_admin_db)
        filepath_user_db = os.path.abspath(os.path.join(filepath, "..", "database", "notifications.db"))
        self._database_users = DatabaseUser(filepath_user_db)

    def init_modules(self):
        self.modules.clear()
        error_module = None
        for module in [(name, cls()) for name, cls in modules.__dict__.items() if isinstance(cls, type)]:
            if module[0] == 'ErrorModule':
                error_module = module[1]
            else:
                self.modules.append(module[1])
        # error module must be added and initialized as last element
        # otherwise commands will not work
        self.modules.append(error_module)
        for module in self.modules:
            module.init_module(self)

    def run(self):
        self.updater.start_polling()
        self.updater.idle()

    def shutdown_graceful(self, signum, frame):
        logging.info("Bot graceful shutdown initialized ...")
        self.statistic.shutdown_graceful(signum, frame)

    def get_help_description(self):
        help_desc = '/help - Shows this help menu \n'
        help_desc += '/notify - Adds/Removes you from the notification list'
        return help_desc

    def start(self, update, context):
        logging.info("Start by User: {} with ID: {} from ChatID: {}".format(
                     update.message.from_user.first_name,
                     update.message.from_user.id,
                     update.message.chat_id))

        user_first_name = update.message.from_user.first_name
        chat_id = update.message.chat_id
        user_id = update.message.from_user.id

        is_in_database = False
        if self._database_admin.is_admin_in_table(user_id):
            logging.info("Loading admin user={} from admin database".format(user_first_name))
            self.group_chat_id, self.admin_chat_id = self._database_admin.get_admin_data(user_id)[0]
            is_in_database = True
        else:
            if self.group_chat_id is None and self.admin_chat_id is None:
                # set primary group chat and bot admin
                logging.info("First time adding user={} to admin database".format(user_first_name))
                self._database_admin.insert_admin(chat_id, user_id)
                self.group_chat_id = chat_id
                self.admin_chat_id = user_id

        if self.group_chat_id != chat_id and not is_in_database:
            return

        chat_id = self.group_chat_id

        if self.running_jobs_dict.get(chat_id, None) is not None:
            return

        # set chat id and admin to modules
        for module in self.modules:
            module.set_chat_ids(self.group_chat_id, self.admin_chat_id, self.database_users.is_user_in_table)

        # add this job only once per chat
        self.running_jobs_dict[chat_id] = 'Active'
        # trigger time is now automatically adapted to Europe/Berlin 00:00
        context.job_queue.run_daily(callback=self.daily, time=get_midnight_time_of_timezone(), context=chat_id)

        context.bot.send_message(
            chat_id=update.message.chat_id,
            text="hiii<3",
            disable_notification=True)

    def stop(self, update, context):
        logging.info("Stop by User: %s with ID: %s",
                     update.message.from_user.first_name,
                     str(update.message.from_user.id))

    def help(self, update, context):
        logging.info("Help by User: %s with ID: %s",
                     update.message.from_user.first_name,
                     str(update.message.from_user.id))

        chat_id = update.message.chat_id
        if not self.database_users.is_user_in_table(chat_id) and \
                (self.group_chat_id != chat_id) and \
                (self.admin_chat_id != chat_id):
            return None

        text = '*Command List*:\n'
        text += self.get_help_description() + '\n'
        for module in self.modules:
            help_desc = module.get_help_description()
            if help_desc != "":
                text += help_desc + '\n'

        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=text,
            parse_mode=ParseMode.MARKDOWN,
            disable_notification=True)

    def ping(self, update, context):
        logging.info("Status by User {} with ID: {}".format(
            update.message.from_user.first_name,
            update.message.from_user.id))

        if self.admin_chat_id != update.message.from_user.id:
            return

        context.bot.send_message(
            chat_id=self.admin_chat_id,
            text="Hey I'm still alive! :)",
            parse_mode=ParseMode.MARKDOWN,
            disable_notification=True)

    def report(self, update, context):
        if self.admin_chat_id != update.message.from_user.id:
            return

       # prevent some telegram errors ?!
        if update is None:
            logging.error("Telegram Update is None")
            return

        if update.message is None:
            logging.error("Telegram Update.Message is None")
            return
        
        if len(context.args) == 0:
            context.bot.send_message(
                update.effective_chat.id,
                "Empy argument provide year as argument e.g. 2020")
            return
        
        year = context.args[0]
        if len(year) > 4:
            context.bot.send_message(
                update.effective_chat.id,
                "Argument is longer than 4 characters.")
            return

        context.bot.send_message(
            self.group_chat_id,
            self._statistic.get_year_report_msg(year),
            parse_mode=ParseMode.MARKDOWN)

    def notify(self, update, context):
        logging.info("Birthdays: birthdays_notification() by User: %s", update.message.from_user.first_name)

        user_id = update.message.from_user.id

        # check if user is allowed to chat
        if not self.database_users.is_user_in_table(user_id) and not self.group_chat_id == update.message.chat_id:
            return

        name = update.message.from_user.first_name
        text = None

        # check if user is in table depending on result add / remove user
        if self.database_users.is_user_in_table(user_id):
            if user_id == update.message.chat_id:
                context.bot.send_message(
                    text="*Notification* \n"
                         "Are you sure you want to *remove* yourself from the notification list? "
                         "This will remove all your notifications and private user rights on this bot "
                         "If you are using this bot within a group you have to add yourself again "
                         "within the group with /notify to get private access back.",
                    chat_id=update.message.chat_id,
                    reply_markup=self.yes_no_markup,
                    parse_mode=ParseMode.MARKDOWN)
                return 0
            else:
                # group chat
                if self.database_users.remove_user((user_id, name)):
                    text = "Removed {} from the notification list".format(name)
        else:
            if self.database_users.insert_user((user_id, name)):
                text = "Added {} to the notification list".format(name)

        if text is None:
            logging.error("Something went wrong during insert/remove ...")
            return

        context.bot.send_message(
            text=text,
            chat_id=update.message.chat_id,
            disable_notification=True
        )

        return ConversationHandler.END

    def yes_no_input(self, update, context):
        logging.info("Handle user input ...")
        query = update.callback_query
        data = query.data

        if data == 'yes':
            logging.info("... user chose yes, removing user from database")
            # remove user from list
            user_id = query.from_user.id
            name = query.from_user.first_name
            if self.database_users.remove_user((user_id, name)):
                logging.info("... successfully removed")
                context.bot.edit_message_text(
                    text="You have been finally removed from the list.",
                    chat_id=query.message.chat_id,
                    message_id=query.message.message_id,
                    parse_mode=ParseMode.MARKDOWN)
        elif data == 'no':
            logging.info("... user chose no, deleting message")
            # delete message
            context.bot.delete_message(
                chat_id=query.message.chat_id,
                message_id=query.message.message_id)
        # end conversation
        context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def daily(self, context):
        logging.info("Running daily check {}".format(get_current_datetime_of_timezone()))
        # run daily statistic check
        self.statistic.check_daily(context)
        # check daily on modules
        for module in self.modules:
            module.check_daily(context)
        # check vip Helmut Schmidt
        from vips.helmut_schmidt import HelmutSchmidt
        helmut = HelmutSchmidt()
        helmut.check_daily(context)
        # remove job, workaround to prevent daylight saving changes
        context.job.schedule_removal()
        # init new job
        context.job.job_queue.run_daily(callback=self.daily,
                                        time=get_midnight_time_of_timezone(),
                                        context=context.job.context)
