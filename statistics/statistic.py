from telegram import ParseMode
from core.common import get_current_datetime_of_timezone
import datetime
import glob
import os
import logging


class Statistic(object):

    def __init__(self):
        self.header = "DATE, MSG_COUNT, GIF_COUNT, IMG_COUNT, JOKE_COUNT, REACTIONS_COUNT"
        # init default values
        self.msg_count = 0
        self.gif_count = 0
        self.img_count = 0
        self.joke_count = 0
        self.reactions_count = 0
        # create statistic folder and init file with header
        self._init_cur_statistic()
        # in case of app restart check if there are already saved values from same day
        self._init_report_values()

    def _init_cur_statistic(self):
        filepath_reports_abs, filepath_cur_statistic, filename = self.create_statistic_file()
        self.filepath_reports_abs = filepath_reports_abs
        self.filepath_cur_statistic = filepath_cur_statistic
        self.filename_cur_statistic = filename

    def _read_value(self, line_items, pos):
        try:
            ret = int(line_items[pos])
        except IndexError as e:
            ret = 0
        except ValueError as e:
            ret = 0
        return ret

    def _init_report_values(self):
        logging.info("Loading old values from file ...")
        # if there is already a file check latest date and load values if date is the same
        with open(self.filepath_cur_statistic, 'r') as file:
            lines = file.readlines()

        line_count = len(lines)

        # return if there is just the header
        if not line_count > 1:
            logging.info("... file contains just header - skipping")
            return

        logging.info("... checking old values")
        # get last line compare date and load values
        line_last = lines[line_count-1]
        today = get_current_datetime_of_timezone()
        line_items = str(line_last).strip('\n').split(',')

        try:
            line_date = datetime.datetime.strptime(line_items[0], "%Y-%m-%d")
        except Exception as e:
            logging.error("Error: {}".format(e))
            logging.error("... loading failed - skipping")
            return

        logging.info("Today: {}".format(today.strftime("%Y-%m-%d")))
        logging.info("File: {}".format(line_date.strftime("%Y-%m-%d")))

        if (line_date.day == today.day) and (line_date.month == today.month) and (line_date.year == today.year):
            logging.info("... loading values")
            try:
                self.msg_count = self._read_value(line_items, 1)
                self.gif_count = self._read_value(line_items, 2)
                self.img_count = self._read_value(line_items, 3)
                self.jokes_count = self._read_value(line_items, 4)
                self.reactions_count = self._read_value(line_items, 5)
                logging.info("... done.")
                logging.info("msg_count = {}, "
                             "gif_count = {}, "
                             "img_count = {}, "
                             "jokes_count = {},"
                             "reactions_count = {}".format(self.msg_count, self.gif_count, self.img_count,
                                                           self.jokes_count, self.reactions_count))
            except Exception as e:
                logging.error("Error: {}".format(e))
                logging.error("... loading values failed - keep defaults.")
                self.reset()
            # remove last line to prevent date duplicates
            logging.info("... removing last line of file")
            with open(self.filepath_cur_statistic, 'w') as file:
                file.writelines(lines[:-1])
            logging.info("... done")
        else:
            logging.info("... not same date - skipping")

    def create_statistic_file(self):
        today = get_current_datetime_of_timezone()
        filename = today.strftime("%Y-%m") + ".txt"
        logging.info("Check file: {}".format(filename))

        filepath_file = os.path.dirname(__file__)
        filepath_reports = os.path.join(filepath_file, "..", "reports")
        filepath_reports_abs = os.path.abspath(filepath_reports)
        if not os.path.exists(filepath_reports_abs):
            logging.info("... create reports folder")
            os.mkdir(filepath_reports_abs)
        else:
            logging.info("... reports folder already exists.")

        filepath_current_statistic = os.path.join(filepath_reports, filename)
        filepath_current_statistic_abs = os.path.abspath(filepath_current_statistic)

        if not os.path.exists(filepath_current_statistic_abs):
            with open(filepath_current_statistic_abs, 'w') as file:
                file.write(self.header + "\n")
            logging.info("... created file.")
        else:
            logging.info("... file already exists.")
        return filepath_reports_abs, filepath_current_statistic_abs, filename

    def shutdown_graceful(self, signum, frame):
        logging.info("... graceful shutdown statistics")
        # Get time Europe/Berlin (default)
        today = get_current_datetime_of_timezone()
        date_str = today.strftime("%Y-%m-%d")
        self.save_daily(date_str)

    def add_one_msg_count(self):
        self.msg_count += 1

    def add_one_gif_count(self):
        self.gif_count += 1

    def add_one_img_count(self):
        self.img_count += 1

    def add_one_joke_count(self):
        self.joke_count += 1

    def add_one_reactions_count(self):
        self.reactions_count += 1

    def reset(self):
        self.msg_count = 0
        self.gif_count = 0
        self.img_count = 0
        self.joke_count = 0
        self.reactions_count = 0

    def get_plus_or_minus_value_str(self, value):
        if value >= 0:
            return "+{}".format(value)
        else:
            # minus sign (-) is automatically added
            return "{}".format(value)

    def get_prev_year_month_str(self, year_month_str):
        datetime_obj = datetime.datetime.strptime(year_month_str, "%Y-%m")
        prev_month = datetime_obj.replace(day=1) - datetime.timedelta(days=1)
        return prev_month.strftime("%Y-%m")

    def generate_comparison(self, msg_count_cur, msg_count_prev,
                            git_count_cur, gif_count_prev,
                            img_count_cur, img_count_prev,
                            joke_count_cur, joke_count_prev,
                            reactions_count_cur, reactions_count_prev):
        msg_comp = self.get_plus_or_minus_value_str(msg_count_cur - msg_count_prev)
        gif_comp = self.get_plus_or_minus_value_str(git_count_cur - gif_count_prev)
        img_comp = self.get_plus_or_minus_value_str(img_count_cur - img_count_prev)
        joke_comp = self.get_plus_or_minus_value_str(joke_count_cur - joke_count_prev)
        reactions_comp = self.get_plus_or_minus_value_str(reactions_count_cur - reactions_count_prev)
        return msg_comp, gif_comp, img_comp, joke_comp, reactions_comp

    def generate_report_month(self, filename):
        if not filename.endswith(".txt"):
            filename += ".txt"
        msg_count, gif_count, img_count, jokes_count, reactions_count = 0, 0, 0, 0, 0
        filepath = os.path.join(self.filepath_reports_abs, filename)
        if os.path.exists(filepath):
            with open(filepath, 'r') as file:
                for cnt, line in enumerate(file):
                    # ignore header
                    if cnt > 0:
                        line_split = str(line).strip('\n').split(',')
                        msg_count += self._read_value(line_split, 1)
                        gif_count += self._read_value(line_split, 2)
                        img_count += self._read_value(line_split, 3)
                        jokes_count += self._read_value(line_split, 4)
                        reactions_count += self._read_value(line_split, 5)
        return msg_count, gif_count, img_count, jokes_count, reactions_count

    def get_monthly_report_msg(self, year_month_str):
        msg_count, gif_count, img_count, joke_count, reactions_count = self.generate_report_month(year_month_str)
        # handle previous month to get + (plus) or - (minus) comparison
        prev_year_month_str = self.get_prev_year_month_str(year_month_str)
        msg_count_p, gif_count_p, img_count_p, joke_count_p, reactions_count_p = \
            self.generate_report_month(prev_year_month_str)
        msg_comp, gif_comp, img_comp, joke_comp, reactions_comp = \
            self.generate_comparison(msg_count, msg_count_p,
                                     gif_count, gif_count_p,
                                     img_count, img_count_p,
                                     joke_count, joke_count_p,
                                     reactions_count, reactions_count_p)
        msg = "Monthly Chat Report: *{}* 📰 \n" \
              "Messages: {} ({})\n" \
              "GIFs: {} ({})\n" \
              "Images: {} ({})\n" \
              "Jokes: {} ({})\n" \
              "Reactions: {} ({}) \n" \
              "#monthlyreport".format(year_month_str,
                                      msg_count, msg_comp,
                                      gif_count, gif_comp,
                                      img_count, img_comp,
                                      joke_count, joke_comp,
                                      reactions_count, reactions_comp)
        return msg

    def generate_report_year(self, year_str):
        msg_count, gif_count, img_count, jokes_count, reactions_count = 0, 0, 0, 0, 0
        files_regex = year_str + "*.txt"
        files_filepath_abs = os.path.join(self.filepath_reports_abs, files_regex)
        reports = [report for report in glob.glob(files_filepath_abs)]
        for file in reports:
            msg, gif, img, jokes, reactions = self.generate_report_month(file)
            msg_count += msg
            gif_count += gif
            img_count += img
            jokes_count += jokes
            reactions_count += reactions
        return msg_count, gif_count, img_count, jokes_count, reactions_count

    def get_year_report_msg(self, year_str):
        msg_count, gif_count, img_count, joke_count, reaction_count = self.generate_report_year(year_str)
        # ugly type conversion :/
        try:
            value = int(year_str) - 1
        except ValueError as e:
            logging.error(e)
            return None
        prev_year_str = str(value)
        # handle comparison to previous year
        msg_count_p, gif_count_p, img_count_p, joke_count_p, reactions_count_p = \
            self.generate_report_year(prev_year_str)
        msg_comp, gif_comp, img_comp, joke_comp, reactions_comp = \
            self.generate_comparison(msg_count, msg_count_p,
                                     gif_count, gif_count_p,
                                     img_count, img_count_p,
                                     joke_count, joke_count_p,
                                     reaction_count, reactions_count_p)
        msg = "Chat Report of *{}* 📰 \n" \
              "Messages: {} ({}) \n" \
              "GIFs: {} ({}) \n" \
              "Images: {} ({}) \n" \
              "Jokes: {} ({}) \n" \
              "Reactions: {} ({}) \n" \
              "Wish you all a Happy New Year 🎉🎊 \n" \
              "#reportoftheyear".format(year_str,
                                        msg_count, msg_comp,
                                        gif_count, gif_comp,
                                        img_count, img_comp,
                                        joke_count, joke_comp,
                                        reaction_count, reactions_comp)
        return msg

    def save_daily(self, date_str):
        # just append current recorded statistic
        with open(self.filepath_cur_statistic, 'a') as file:
            file.write("{},{},{},{},{},{} \n".format(date_str, self.msg_count, self.gif_count,
                                                     self.img_count, self.joke_count, self.reactions_count))

    def check_daily(self, context):
        logging.info("Daily check statistic ...")

        # midnight Europe/Berlin time 00:00
        today = get_current_datetime_of_timezone()
        # since today is already set to new day subtract one day to get previous day
        record_last_day = today - datetime.timedelta(days=1)
        # save date timestamp of last day with format: YYYY-MM-DD
        date_str = record_last_day.strftime("%Y-%m-%d")

        first_of_month = today.replace(day=1)

        # save counters of day
        self.save_daily(date_str)
        # reset counters for upcoming day
        self.reset()

        # first day of new month
        if today.day != first_of_month.day:
            return

        generate_year_report = False
        logging.info("Date: {}".format(today))
        logging.info("... generating report")

        # subtract one day of fist day of month to get last month
        datetime_last_month = first_of_month - datetime.timedelta(days=1)

        # first of January
        if today.month == 1:
            # generate year statistic
            report_year = self.get_year_report_msg(datetime_last_month.strftime("%Y"))
            if report_year is not None:
                logging.info("... sending year report")
                # send year report
                context.bot.send_message(
                    chat_id=context.job.context,
                    text=report_year,
                    parse_mode=ParseMode.MARKDOWN,
                    disable_notification=True)

        # send monthly statistic
        report_month = self.get_monthly_report_msg(datetime_last_month.strftime("%Y-%m"))

        logging.info("... sending monthly report")
        # send monthly report
        context.bot.send_message(
            chat_id=context.job.context,
            text=report_month,
            parse_mode=ParseMode.MARKDOWN,
            disable_notification=True)

        # create new statistic file for new month
        self._init_cur_statistic()
