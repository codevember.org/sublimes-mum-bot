# Sublimes-Mum-Bot

## About
Sublimes-Mum-Bot is a Telegram bot living in our group chat. The purpose is to remind us of birthdays and to entertain
us with certain functionalities.

### Functions
* Sends picture + happy birthday message in case someone has birthday
* Can send jokes with the `/joke` command
* Creates based on the month and year chat reports about amount of messages, images, gifs or jokes.
* Text based reactions if a users types in a special word

### Dependencies
* Based on Python 3.8 and packages

### Get Started
```
python3 -m pip install python-telegram-bot
python3 -m pip install pyAesCrypt
python3 -m pip install requests 
python3 -m pip install pytz
python3 -m pip install tzlocal
```

### Usage (Server)
Start the bot
```
python3 sublimes-mum-bot.py --token token.txt --data template.json
```

You will be asked for a password to encrypt all personal information. If there is already a `.aes` it can be loaded instead of the `.json` file. If the `.aes` file has been created you still have to remove the `.json` file from the disk in order to vanish all personal information on your disk.

```
python3 sublimes-mum-bot.py --token token.txt --data template.aes
```

Template file which is used to load or add new people, file is included (template.json):
```
{
  "persons": [
    {
      "name": "Max",
      "birthday": "01-01-1991"
    },
    {
      "name": "Alexa",
      "birthday": "02-02-1992"
    },
    ...
  ]
}
```

A daily function is triggered which will update the jokes, reactions and ping the url's of the birthday pictures if they are still available. In addition the bot will send a reminder message to all users which added themself to the notification list if someone has birthday.
* add jokes here: `resources/jokes.py`
* add new birthday images: `resources/images.py`
* add new reactions here: `resources/reactions.py`

### Usage (Telegram)
Start the bot by writting `/start` in the chat. The one who starts the bot for the first time automatically becomes the admin. This bot will only communicate within the added group where the bot gets started. It will also communicate with persons who added themself with the command `/notify` to the notification list.

**Command List**:
```
/help - Shows this help menu
/joke - Unleash the fun
/notify - Adds/Removes you from the notification list
/birthdays - Show Birthday list
/birthdays <name> - Show birthday results found via user input
```

### Status
If you have any questions, feel free to ask!

Copyright (c) CodevemberTeam

