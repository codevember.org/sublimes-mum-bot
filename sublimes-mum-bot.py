from bot.telegram_bot import TelegramBot
import getpass
import pyAesCrypt
import argparse
import logging
import json
import sys
import os


def get_password():
    if args.pw:
        return args.pw
    else:
        pw = getpass.getpass(prompt="Enter encryption password: ")
        return pw


def load_token():
    logging.info("Loading token ...")
    if str(args.token).endswith(".txt"):
        # open and load token from file
        with open(file=args.token, mode='r') as myfile:
            token = myfile.read().replace('\n', '')
    else:
        token = args.token
    logging.info("Loading token SUCCESSFULLY")
    return token


def load_data():
    # load data
    logging.info("Loading data ...")
    if str(args.data).endswith(".json"):
        logging.info("... from .json file, encryption comes afterwards")
        # load resources from json file and encrypt
        with open(args.data) as f:
            data = json.load(f)
        filename_aes = args.data[:-4] + "aes"
        pyAesCrypt.encryptFile(args.data, filename_aes, args.pw, 64 * 1024)
        if os.path.exists(filename_aes):
            os.remove(args.data)
    elif str(args.data).endswith(".aes"):
        logging.info("... from .aes file, decrypting file")
        json_file_name = args.data[:-3] + "json"
        pyAesCrypt.decryptFile(args.data, json_file_name, args.pw, 64 * 1024)
        # load resources from decrypted file and remove it afterwards
        with open(json_file_name) as f:
            data = json.load(f)
        os.remove(json_file_name)
    else:
        logging.error("No .json or .aes resources file denoted")
        sys.exit(1)
    logging.info("Loading data SUCCESSFULLY")
    return data


def main():
    # start bot conversation
    bot = TelegramBot(load_token(), load_data())
    bot.run()


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)
    parser = argparse.ArgumentParser(description="Telegram Bot")
    parser.add_argument("--token", metavar="token", type=str, nargs="?", required=True,
                        help="Token or path to .txt including the token id")
    parser.add_argument("--data", metavar="data & bot settings", type=str, nargs="?", required=True,
                        help="path to .json file or decrypted .aes file")
    parser.add_argument("--pw", metavar="password to de- or encrypt the data file", type=str, nargs="?", required=False,
                        help="(optional) pw to de- or encrypt the data file. If not set will be asked in cmd line")
    args = parser.parse_args()
    args.pw = get_password()
    main()

