import pyAesCrypt
import getpass
import argparse
import sys


def get_password(prompt="Enter Password: ") -> str:
    pw = getpass.getpass(prompt)
    return pw


def encrypt() -> bool:
    if args.encrypt.endswith(".json"):
        try:
            encrypt_pw = get_password("Enter decryption password: ")
            filename_aes = args.encrypt[:-4] + "aes"
            pyAesCrypt.encryptFile(args.encrypt, filename_aes, encrypt_pw, 64 * 1024)
        except Exception as e:
            print(e)
            return False
    else:
        raise Exception("Wrong file format, expected a .json file as input.")
    return True


def decrypt() -> bool:
    if args.decrypt.endswith(".aes"):
        try:
            decrypt_pw = get_password("Enter decryption password: ")
            filename_json = args.decrypt[:-3] + "json"
            pyAesCrypt.decryptFile(args.decrypt, filename_json, decrypt_pw, 64 * 1024)
        except Exception as e:
            print(e)
            return False
    else:
        raise Exception("Wrong file format, expected a .aes file as input.")
    return True


def main() -> int:
    if args.encrypt:
        retval = encrypt()
    elif args.decrypt:
        retval = decrypt()
    else:
        parser.print_help()
        retval = 0

    if retval:
        return 0
    else:
        return 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Helper to encrypt or decrpyt a .json / .aes file,"
                                                 "plus adding a new person to the list.")
    parser.add_argument('-e', '--encrypt', metavar="encrypt", type=str, help="Encrypts a file", required=False)
    parser.add_argument('-d', '--decrypt', metavar="decrypt", type=str, help="Decrypts a file", required=False)
    args = parser.parse_args()
    sys.exit(main())
