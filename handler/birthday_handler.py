from core.group import Group
import datetime
import random
import logging
from core.common import is_valid
import importlib
from resources import images


class BirthdayHandler(object):

    def __init__(self, data):
        logging.info("Init {}".format(self.__class__.__name__))
        # init group and load data from .json file
        self._group = Group(data)
        self._min_idx = 0
        self._max_idx = len(self._group.persons) - 1
        self._cur_idx = self.compute_first_birthday_index()
        # check if images are valid
        self._birthday_images = [url for url in images.birthday_images if is_valid(url)]
        self._year = datetime.datetime.today().year

    def update_current_index(self):
        self._cur_idx += 1
        if self._cur_idx > self._max_idx:
            self._cur_idx = self._min_idx

    def reset_current_index(self):
        self._cur_idx = self.compute_first_birthday_index()

    def update_birthday_images(self):
        importlib.reload(images)
        self._birthday_images = None
        self._birthday_images = [url for url in images.birthday_images if is_valid(url)]

    def get_random_birthday_image_url(self):
        if len(self._birthday_images) > 0:
            return random.choice(self._birthday_images)
        return None

    def get_current_birthday_person(self):
        return self._group.persons[self._cur_idx]

    def get_next_birthday_person(self):
        self._cur_idx += 1
        if self._cur_idx > self._max_idx:
            self._cur_idx = self._min_idx
        return self._group.persons[self._cur_idx]

    def get_prev_birthday_person(self):
        self._cur_idx -= 1
        if self._cur_idx < self._min_idx:
            self._cur_idx = self._max_idx
        return self._group.persons[self._cur_idx]

    def sort_group(self, descending=False):
        self._group.sort_group_after_birthdays(descending)

    def get_birthday_persons(self, date):
        birthday_persons = []
        for person in self._group.persons:
            if (person.birthday_datetime.day == date.day) and (person.birthday_datetime.month == date.month):
                birthday_persons.append(person)
                person.update_age(date.year)
        return birthday_persons

    def get_year(self, datetime_today, datetime_birthday):
        # update year
        if datetime_birthday.month < datetime_today.month:
            return self._year + 1
        if datetime_birthday.month == datetime_today.month:
            if datetime_birthday.day <= datetime_today.day:
                return self._year + 1
        return self._year

    def get_birthday_by_name(self, name):
        search_name = str(name).lower().replace(' ', '')
        # min search input search len should be 2
        is_min_search_name_len = len(search_name) > 2
        names_list = []
        # add person to list if name is equal or substring
        for person in self._group.persons:
            # check first name
            is_in_name = (search_name == person.name.lower()) or \
                         (is_min_search_name_len and (search_name in person.name.lower()))
            # append person if name is equal
            if is_in_name:
                names_list.append(person)
        return names_list

    def compute_first_birthday_index(self):
        today = datetime.date.today()
        last_date = datetime.datetime.strptime("31-12-1337", "%d-%m-%Y")
        result = 0
        # iterate through key list and get next idx
        for idx, p in enumerate(self._group.persons):
            if (today.month == p.birthday_month) and (today.day < p.birthday_day):
                # check if there is someone who has in the coming days birthday
                return idx
            elif today.month < p.birthday_month:
                # check next month if nobody has in current month
                return idx
            elif today.month > p.birthday_month:
                # check for new year example cur = 31.12 bd = 1.1 => get min date
                if (last_date.month > p.birthday_month) and (last_date.day > p.birthday_day):
                    last_date = p.birthday_datetime
                    result = idx
        return result


