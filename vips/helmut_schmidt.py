from core.person import Person
from core.common import get_current_datetime_of_timezone
import datetime
import logging


class HelmutSchmidt(Person):

    def __init__(self):
        super().__init__('Helmut Schmidt', '23-12-1918')

        self._death_str = '10-11-2015'
        self._death_datetime = datetime.datetime.strptime(self._death_str, '%d-%m-%Y')
        self._images = ['https://upload.wikimedia.org/wikipedia/commons/9/9a/Verteidigungsminister_Helmut_Schmidt.jpg',
                        'http://cdn4.spiegel.de/images/image-652607-galleryV9-hjkd-652607.jpg']

    @property
    def image(self):
        return self._images

    @property
    def died_str(self):
        return self._death_str

    @property
    def died_datetime(self):
        return self._death_datetime

    def check_daily(self, context):
        logging.info("... Helmut Schmidt VIP daily check")
        today = get_current_datetime_of_timezone()
        if self.birthday_datetime.day == today.day and self.birthday_datetime.month == today.month:
            # birthday
            context.bot.send_photo(
                chat_id=context.job.context,
                photo=self._images[0],
                caption="Du musst RAUCHEN! Happy Birthday Helmut Schmidt!")
        elif self._death_datetime.day == today.day and self._death_datetime.month == today.month:
            # died
            context.bot.send_photo(
                chat_id=context.job.context,
                photo=self._images[1],
                caption="RIP - Durfte überall rauchen!")
