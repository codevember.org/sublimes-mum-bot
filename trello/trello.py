import requests
import json
import os


class Trello(object):

    """
        The class contains the logic to create cards on our trello board
    """

    def __init__(self):
        self._board_id = "TIuKMHNs"
        self._list_name = "SublimeChat"
        self._list_id = "6028f0952ca5223a23a41d62"
        self._api_key, self._api_token = self.load_api_keys()

    @staticmethod
    def load_api_keys():
        api_key_filename = "trello.json"
        api_key_filepath = os.path.join(os.path.dirname(os.path.dirname(__file__)), api_key_filename)
        with open(api_key_filepath, 'r') as json_trello_api_keys:
            json_content = json.load(json_trello_api_keys)
            return json_content['api_key'], json_content['api_token']

    def _create_card_query(self, name, desc):
        return {'key': self._api_key,
                'token': self._api_token,
                'name': name,
                'desc': desc,
                'pos': 'top',
                'idList': self._list_id,
                }

    def _create_card_request(self, query):
        url_cards = "https://api.trello.com/1/cards"
        return requests.request(
            "POST",
            url_cards,
            params=query)

    def create_card(self, name, desc):
        query = self._create_card_query(name, desc)
        response = self._create_card_request(query)
        return response

    def add_url_attachment_to_card(self, card_id, url):
        url_attachment = f"https://api.trello.com/1/cards/{card_id}/attachments"
        headers = {"Accept": "application/json"}
        query = {
            'id': card_id,
            'key': self._api_key,
            'token': self._api_token,
            'url': url
        }
        response = requests.request("POST", url_attachment, headers=headers, params=query)
        return response
